{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation {
  name = "emacs-config";
  src = ./.;
  buildInputs = [ pkgs.emacs ];
  buildPhase = ''
    patchShebangs tangle.el
    ./tangle.el
    grep -oP '(?<=use-package )[a-zA-Z0-9\-\_]+' README.org | sort > package-list.txt
  '';
  installPhase = ''
    mkdir $out
    install -t $out init.el package-list.txt
  '';
}
