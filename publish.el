#!/usr/bin/env -S emacs -Q --script
;; This script was shamelessly stolen from: https://systemcrafters.net/publishing-websites-with-org-mode/building-the-site/

;; Set the package installation directory so that packages aren't stored in the
;; ~/.emacs.d/elpa path.
(require 'package)
(setq package-user-dir (expand-file-name "./.packages"))
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

;; Initialize the package system
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Install dependencies
(package-install 'htmlize)

;; Load the publishing system
(require 'ox-publish)

;; Just add class definitions to the source code blocks.
;; We'll style them via a separate CSS file that's included with our theme.
(setq org-html-htmlize-output-type 'css)

;; Define the publishing project
(setq org-publish-project-alist
      (list
       (list "org-site:main"
             :recursive t
             :base-directory "."
             :publishing-function 'org-html-publish-to-html
             :publishing-directory "./public"
             :with-author t             ;; Don't include author name
             :with-creator t            ;; Include Emacs and Org versions in footer
             :with-toc t                ;; Include a table of contents
             :section-numbers nil       ;; Don't include section numbers
             :time-stamp-file t         ;; Don't include time stamp in file
             :auto-sitemap t
             ;; Add an index page.
             :sitemap-filename "index.org"
             :sitemap-file-entry-format "%d *%t*"
             :sitemap-style 'list
             :sitemap-sort-files 'anti-chronologically
             )
       )
      )

;; Generate the site output
(org-publish-all t)

(message "Build complete!")
