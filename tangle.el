#!/usr/bin/env -S emacs -Q --script

(require 'org)

(org-babel-tangle-file "README.org" "init.el")
