{
  description = "Emacs configuration";

  inputs.nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";

  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages."${system}";

      emacs-config = import ./default.nix { inherit pkgs; };

      myEmacs = (pkgs.emacsPackagesFor pkgs.emacs).emacsWithPackages (epkgs:
        let
          packageNames  = pkgs.lib.strings.splitString "\n" (builtins.readFile "${emacs-config}/package-list.txt");
          packageByName = name: epkgs . "${name}";
          packageExists = name: epkgs ? "${name}";
        in
          [ epkgs.treesit-grammars.with-all-grammars ] ++ (map packageByName (builtins.filter packageExists packageNames)));

      sandboxed-emacs = pkgs.writeShellScriptBin "sandboxed-emacs" ''
        export HOME="$(${pkgs.coreutils-full}/bin/mktemp -d)";
        mkdir "$HOME/.emacs.d"
        ln -s "${emacs-config}/init.el" "$HOME/.emacs.d/init.el"
        ${myEmacs}/bin/emacs
      '';
    in
      {
        packages.${system} = {
          # Just the config file (and a list of required packages).
          config = emacs-config;
          # Emacs with required packages pre-installed.
          withMyPackages = myEmacs;
          # For testing my Emacs + config in a temporary home directory.
          default = sandboxed-emacs;
        };
      };
}
